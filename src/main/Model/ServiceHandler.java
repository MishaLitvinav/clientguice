package Model;


import com.google.inject.Inject;

public class ServiceHandler {
    private final iService service;

    @Inject
    public ServiceHandler(iService service) {
        this.service = service;
    }

    public void run(){
        service.ConnectionAction();
    }
}

