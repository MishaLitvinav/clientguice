package Model;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import ViewModel.Singleton;

public class ServiceGuiceModule extends AbstractModule {

    private Singleton status;

    public ServiceGuiceModule() {
        status = status.getInstance();
    }

    @Provides
    public iService getService() {
        if(status.getCurrentWindow() == "Register"){
            return new Register();
        }
        if(status.getCurrentWindow() == "LogIn"){
            return new LogIn();
        }
        return null;
    }
}
