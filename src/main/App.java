import Model.ServiceGuiceModule;
import Model.ServiceHandler;
import ViewModel.ResourceGuiceModule;
import ViewModel.ResourceHandler;
import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {
    private static Scene scene;

    public static void main(String[] args) throws Exception {
        Injector injector = Guice.createInjector(new ResourceGuiceModule());
        ResourceHandler handler = injector.getInstance(ResourceHandler.class);
        Parent root = handler.getScene();

        scene = new Scene(root);
        launch(args);

        Injector guice = Guice.createInjector(new ServiceGuiceModule());
        ServiceHandler service = guice.getInstance(ServiceHandler.class);
        service.run();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
