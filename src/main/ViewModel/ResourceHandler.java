package ViewModel;

import com.google.inject.Inject;
import javafx.scene.Parent;

public class ResourceHandler {
    private iParentRootResource PRR;

    @Inject
    public ResourceHandler(iParentRootResource PRR) {
        this.PRR = PRR;
    }

    public Parent getScene() throws Exception {
        return PRR.getResource();
    }
}
