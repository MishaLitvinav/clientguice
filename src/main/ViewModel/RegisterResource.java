package ViewModel;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.net.URL;

public class RegisterResource implements iParentRootResource {
    public Parent getResource() throws Exception {
        URL path = LogInResource.class.getClassLoader().getResource("View/Register.fxml");
        System.out.println("Client wants to have a register window");
        return (FXMLLoader.load(path));
    }
}
