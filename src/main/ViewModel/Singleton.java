package ViewModel;

public class Singleton {
    private static Singleton window = null;
    //default = null | LogIn => Register | Register => LogIn
    private String currentWindow = null;

    private Singleton() { }

    public static Singleton getInstance(){
        if(window == null){
            window = new Singleton();
        }
        return window;
    }

    public void setCurrentWindow(String current){
        this.currentWindow = current;
    }

    public String getCurrentWindow(){
        return this.currentWindow;
    }
}
