package ViewModel;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.net.URL;

public class LogInResource implements iParentRootResource {
    public Parent getResource() throws Exception {
        URL path = LogInResource.class.getClassLoader().getResource("View/login.fxml");
        System.out.println("Client wants to have a login window");
        return (FXMLLoader.load(path));
    }
}
