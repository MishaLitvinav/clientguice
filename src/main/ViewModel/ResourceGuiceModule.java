package ViewModel;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

public class ResourceGuiceModule extends AbstractModule {
    private Singleton window;

    public ResourceGuiceModule() {
        window = Singleton.getInstance();
    }

    @Provides
    public iParentRootResource getRoot(){
        if(window.getCurrentWindow() == "LogIn") {
            window.setCurrentWindow("Register");
            return new RegisterResource();
        }
        if(window.getCurrentWindow() == "Register") {
            window.setCurrentWindow("LogIn");
            return new LogInResource();
        }
        if(window.getCurrentWindow() == null){
            window = Singleton.getInstance();
            window.setCurrentWindow("LogIn");
            return new LogInResource();
        }
        return null;
    }
}

