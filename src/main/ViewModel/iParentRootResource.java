package ViewModel;

import javafx.scene.Parent;

public interface iParentRootResource {

    Parent getResource() throws Exception;

}
